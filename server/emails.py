#!/usr/bin/env python3
import os
from datetime import datetime
from flask import render_template
from flask_emails import Message

client = os.getenv('CLIENT_ORIGIN')
mailer = ('Mailer', os.getenv('MAIL_SENDER'))

def send_startup_email():
    context = { 'time' : datetime.utcnow() }
    plain = render_template('emails/startup.txt', **context)
    msg = Message(
        subject='Auth Server Up',
        mail_from=mailer,
        text=plain
    )
    return msg.send(to=os.getenv('DEVELOPER_EMAIL'))
    